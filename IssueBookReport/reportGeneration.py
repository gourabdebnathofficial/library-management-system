import io
import tornado.web
import xlsxwriter
from database import Connection
from datetime import datetime

class getIssuebooksExcelHandler(tornado.web.RequestHandler):
    async def get(self):
        issuedBooksTable = Connection.db['issued_books']
        usersTable = Connection.db['users']
        booksTable = Connection.db['books']

        cursor = issuedBooksTable.find({}, {"_id": 0, "user_id": 1, "ISBN": 1, "library_id": 1, "issue_date": 1, "return_date": 1, "returned": 1})
        data = await cursor.to_list(length=None)

        output = io.BytesIO()

        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        worksheet = workbook.add_worksheet()

        header_format = workbook.add_format({'bold': True, 'align': 'center', 'valign': 'vcenter'})
        
        highlight_format = workbook.add_format({'bg_color': '#FFC7CE', 'font_color': '#9C0006'})

        headers = ['Username', 'Book Name', 'Library ID', 'Issue Date', 'Return Date', 'Returned']
        for col_num, header in enumerate(headers):
            worksheet.write(0, col_num, header, header_format)

        for row_num, record in enumerate(data, start=1):
            user_details = await usersTable.find_one({'library_id': record.get('library_id')})
            book_details = await booksTable.find_one({'ISBN': record.get('ISBN')})

            username = user_details.get('name', 'Unknown') if user_details else 'Unknown'
            book_name = book_details.get('title', 'Unknown') if book_details else 'Unknown'

            worksheet.write(row_num, 0, username)
            worksheet.write(row_num, 1, book_name)
            worksheet.write(row_num, 2, record.get("library_id", ""))
            
            issue_date = record.get("issue_date")
            return_date = record.get("return_date")
            
            if isinstance(issue_date, (int, float)):
                issue_date = datetime.fromtimestamp(issue_date)
            if isinstance(return_date, (int, float)):
                return_date = datetime.fromtimestamp(return_date)
            
            worksheet.write(row_num, 3, issue_date.strftime('%Y-%m-%d') if issue_date else "")
            worksheet.write(row_num, 4, return_date.strftime('%Y-%m-%d') if return_date else "")
            
            returned = record.get("returned", False)
            worksheet.write(row_num, 5, 'Yes' if returned else 'No')
            
            if not returned:
                worksheet.set_row(row_num, None, highlight_format)

        worksheet.set_column(0, 5, 20)

        workbook.close()

        output.seek(0)

        self.set_header('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        self.set_header('Content-Disposition', 'attachment; filename=issue_book_report.xlsx')

        self.write(output.getvalue())
        self.finish()
