import json
import random
import smtplib
import tornado.ioloop
import tornado.web
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from datetime import datetime, timedelta
from database import Connection

OTP_EXPIRATION_TIME = 5 

SMTP_SERVER = "smtp.gmail.com"
SMTP_PORT = 587
SMTP_USER = "gourabdebnathofficial@gmail.com"
SMTP_PASSWORD = "pecu sexm maej ldla"

def send_email(to_email, subject, body):
    msg = MIMEMultipart()
    msg['From'] = SMTP_USER
    msg['To'] = to_email
    msg['Subject'] = subject

    msg.attach(MIMEText(body, 'plain'))

    try:
        server = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
        server.starttls()
        server.login(SMTP_USER, SMTP_PASSWORD)
        text = msg.as_string()
        server.sendmail(SMTP_USER, to_email, text)
        server.quit()
        print(f"Email sent to {to_email}")
    except Exception as e:
        print(f"Failed to send email: {e}")

class RequestPasswordResetHandler(tornado.web.RequestHandler):
    SUPPORTED_METHODS = ('POST')
    usersTable = Connection.db['users']

    async def post(self):
        code = 4000
        status = False
        message = ''
        
        try:
            self.request.arguments = json.loads(self.request.body.decode())
            email = self.request.arguments.get('email')
            if not email:
                code = 4024
                message = 'Email field is missing'
                raise ValueError(message)
            
            user = await self.usersTable.find_one({'email': email})
            if user:
                otp = random.randint(1000, 9999)
                expiration_time = datetime.utcnow() + timedelta(minutes=OTP_EXPIRATION_TIME)
                
                await self.usersTable.update_one(
                    {'_id': user['_id']},
                    {'$set': {'otp': otp, 'otp_expiration': expiration_time}}
                )
                
                send_email(email, "Password Reset OTP", f"Your OTP is {otp}")
                
                status = True
                code = 2000
                message = 'OTP sent to your email'
            else:
                code = 4043
                message = 'Email not found'
                raise ValueError(message)
        
        except Exception as e:
            if not message:
                code = 5000
                message = 'Internal Server Error'
            print(f"Error: {e}")
        
        response = {
            'code': code,
            'message': message,
            'status': status,
            'otp': otp
        }
        print(response)
        self.write(response)
        self.finish()