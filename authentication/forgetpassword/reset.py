import json
import bcrypt
import tornado.ioloop
import tornado.web
from datetime import datetime
from database import Connection


class ResetPasswordHandler(tornado.web.RequestHandler):
    SUPPORTED_METHODS = ('POST')
    usersTable = Connection.db['users']

    async def post(self):
        response = {'code': 4000, 'message': 'Bad request', 'status': False}

        try:
            request_data = json.loads(self.request.body.decode())
            email = request_data.get('email')
            otp = request_data.get('otp')
            new_password = request_data.get('new_password')

            if not email or not otp or not new_password:
                response.update({'code': 4024, 'message': 'Email, OTP, and new password fields are required'})
                self.write(response)
                return

            user = await self.usersTable.find_one({'email': email})

            if user:
                stored_otp = user.get('otp')
                otp_expiration = user.get('otp_expiration')

                print(f"Stored OTP: {stored_otp}")
                print(f"OTP expiration time: {otp_expiration}")

                if str(stored_otp) == str(otp):
                    if otp_expiration and datetime.utcnow() <= otp_expiration:
                        hashed_password = bcrypt.hashpw(new_password.encode('utf-8'), bcrypt.gensalt()).decode('utf-8')
                        
                        await self.usersTable.update_one(
                            {'_id': user['_id']},
                            {'$set': {'password': hashed_password}, '$unset': {'otp': "", 'otp_expiration': ""}}
                        )
                        
                        response.update({'code': 2000, 'message': 'Password reset successfully', 'status': True})
                    else:
                        response.update({'code': 4030, 'message': 'OTP expired'})
                else:
                    response.update({'code': 4043, 'message': 'Invalid OTP'})
            else:
                response.update({'code': 4043, 'message': 'Email not found'})

        except json.JSONDecodeError:
            response.update({'code': 4000, 'message': 'Invalid JSON format'})
        except Exception as e:
            print(f"Error: {e}")
            response.update({'code': 5000, 'message': 'Internal Server Error'})

        self.write(response)
        self.finish()

