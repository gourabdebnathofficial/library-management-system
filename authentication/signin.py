import json
import jwt
import tornado.ioloop
import tornado.web
import bcrypt
from datetime import datetime, timedelta
from database import Connection
import time

SECRET_KEY = "Xlayer.in"

class SignInHandler(tornado.web.RequestHandler):
    SUPPORTED_METHODS = ('POST')

    usersTable = Connection.db['users']
    sessionTable = Connection.db['session']

    async def post(self):
        code = 4000
        status = False
        result = []
        message = ''
        
        try:
            try:
                self.request.arguments = json.loads(self.request.body.decode())
            except json.JSONDecodeError:
                code = 1001
                message = 'Invalid JSON'
                raise ValueError(message)

            mLibId = self.request.arguments.get('library_id')
            if not mLibId:
                code = 4024
                message = 'Library ID field is missing'
                raise ValueError(message)

            password = self.request.arguments.get('password')
            if not password:
                code = 4030
                message = 'Password field is missing'
                raise ValueError(message)
            
            user = await self.usersTable.find_one({'library_id': mLibId})
            if user and bcrypt.checkpw(password.encode('utf-8'), user['password'].encode('utf-8')):
                session_data = {
                    'user_id': str(user['_id']),
                    'login_time': int(time.time()),
                    'logout_time': None,
                    'duration': None
                }
                result_in = await self.sessionTable.insert_one(session_data)
                session_id = str(result_in.inserted_id)

                payload = {
                    'user_id': str(user['_id']),
                    '_id': session_id,
                    'exp': datetime.utcnow() + timedelta(hours=24)
                }   
                token = jwt.encode(payload, SECRET_KEY, algorithm="HS256")

                status = True
                code = 2000
                message = 'Sign in successful'
                result.append({
                    'token': token,
                    'role': user.get('role', 'user')
                })
            else:
                code = 4043
                message = 'Invalid credentials'
                raise ValueError(message)
        
        except Exception as e:
            if not message:
                code = 5000
                message = 'Internal Server Error'
            print(f"Error: {e}")
        
        response = {
            'code': code,
            'message': message,
            'status': status
        }
        if result:
            response['result'] = result
        
        self.write(response)
        self.finish()
