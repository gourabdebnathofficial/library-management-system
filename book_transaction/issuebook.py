import json
from datetime import datetime

from bson import ObjectId
from database import Connection
import tornado

from tokenauth import xenProtocol

class IssueBookHandler(tornado.web.RequestHandler):
    SUPPORTED_METHODS = ('POST')
    booksTable = Connection.db['books']
    issuedBooksTable = Connection.db['issued_books']
    usersTable = Connection.db['users']
    
    @xenProtocol
    async def post(self):
        code = 4000
        status = False
        result = []
        message = ''
        try:
            user = await self.usersTable.find_one({'_id': ObjectId(self.user_id)})
            if not user:
                message = 'User not found'
                code = 4002
                raise tornado.web.HTTPError(400, reason=message)

            mUserRole = user.get('role')
            if mUserRole != 'admin':
                message = 'Unauthorized access'
                code = 4030
                raise tornado.web.HTTPError(403, reason=message)
            try:
                self.request.arguments = json.loads(self.request.body.decode())
            except json.JSONDecodeError:
                code = 1001
                message = 'Invalid JSON'
                raise Exception
            
            mIsbn = self.request.arguments.get('ISBN')
            if not mIsbn:
                message = 'ISBN field is required'
                code = 4031
                raise Exception
            if not isinstance(mIsbn, str):
                message = 'ISBN must be a string'
                code = 4034
                raise Exception
            
            mStuLibId = self.request.arguments.get('library_id')
            if not mStuLibId:
                message = 'library ID field is required'
                code = 4031
                raise Exception
            if not isinstance(mStuLibId, str):
                message = 'Library ID must be a string'
                code = 4034
                raise Exception

            mIssueDt = datetime.now()
            mRtnDt = self.request.arguments.get('return_date')
            if not mRtnDt:
                message = 'Return Date field is required'
                code = 4031
                raise Exception
            if not isinstance(mRtnDt, str):
                message = 'Return Date must be a string (YYYY-MM-DD)'
                code = 4034
                raise Exception
            
            mRtnDt_dt = datetime.strptime(mRtnDt, '%Y-%m-%d')
            
            if mRtnDt_dt < mIssueDt:
                message = 'Return Date cannot be before the current date'
                code = 4035
                raise Exception
            
            mRtnDt_timestamp = mRtnDt_dt.timestamp()

            student = await self.usersTable.find_one({'library_id': mStuLibId})
            if not student:
                code = 4029
                message = 'Wrong Library ID'
                raise Exception
            mStuName = student.get('name')

            book = await self.booksTable.find_one({'ISBN': mIsbn})
            if not book:
                code = 4029
                message = 'Wrong ISBN number'
                raise Exception
            
            if book['quantity'] <= 0:
                code = 4031
                message = 'Book is out of stock'
                raise Exception
            
            update_result = await self.booksTable.update_one(
                {'ISBN': mIsbn},
                {'$inc': {'quantity': -1}}
            )
            
            if update_result.modified_count > 0:
                issue_record = {
                    'ISBN': mIsbn,
                    'library_id': mStuLibId,
                    'name': mStuName,
                    'issue_date': int(mIssueDt.timestamp()),
                    'return_date': mRtnDt_timestamp,
                    'returned': False
                }
                await self.issuedBooksTable.insert_one(issue_record)
                
                status = True
                code = 4074
                message = 'Book issued successfully'
            else:
                message = 'Failed to issue book'
                code = 4083
                raise Exception
        
        except:
            if not message:
                code = 5000
                message = 'There is some issue or any internal server error'
                raise Exception
        
        response = {
            'code': code,
            'message': message,
            'status': status
        }
        try:
            if len(result):
                response['result'] = result
            print('Issued Book', response) 
            self.write(response)
            self.finish()
        except:
            message = 'There is some issue'
            code = 4124
            raise Exception
