import json
from datetime import datetime

from bson import ObjectId
from database import Connection
import tornado

from tokenauth import xenProtocol

class ReturnBookHandler(tornado.web.RequestHandler):
    SUPPORTED_METHODS = ('POST')
    booksTable = Connection.db['books']
    issuedBooksTable = Connection.db['issued_books']
    usersTable = Connection.db['users']
    
    async def get_user_role(self, user_id):
        user = await self.usersTable.find_one({'_id': ObjectId(user_id)})
        if user:
            return user.get('role')
        return None
    @xenProtocol
    async def post(self):
        code = 4000
        status = False
        result = []
        message = ''

        try:
            user = await self.usersTable.find_one({'_id': ObjectId(self.user_id)})
            if not user:
                message = 'User not found'
                code = 4002
                raise tornado.web.HTTPError(400, reason=message)

            mUserRole = user.get('role')
            if mUserRole != 'admin':
                message = 'Unauthorized access'
                code = 4030
                raise tornado.web.HTTPError(403, reason=message)
            try:
                self.request.arguments = json.loads(self.request.body.decode())
            except json.JSONDecodeError:
                code = 1001
                message = 'Invalid JSON'
                raise Exception
            
            mIsbn = self.request.arguments.get('ISBN')
            if not mIsbn:
                message = 'ISBN field is required'
                code = 4031
                raise Exception
            if type(mIsbn) != str:
                message = 'ISBN must be string'
                code = 4034
                raise Exception
            
            mStuLibId = self.request.arguments.get('library_id')
            if not mStuLibId:
                message = 'library ID field is required'
                code = 4031
                raise Exception
            if type(mStuLibId) != str:
                message = 'Library ID must be string'
                code = 4034
                raise Exception
            

            issued_record = await self.issuedBooksTable.find_one({
                'ISBN': mIsbn, 
                'library_id': mStuLibId
            })
            if not issued_record:
                code = 4032
                message = 'No active issued record found for this book and student or Wrong inputs'
                raise Exception
            
            
            
            mRtnDt_timestamp = datetime.now().timestamp()
            
            update_result = await self.issuedBooksTable.update_one(
                {'ISBN': mIsbn, 'library_id': mStuLibId, 'returned': False},
                {'$set': {'returned': True, 'return_date': int(mRtnDt_timestamp)}}
            )
            if update_result.modified_count > 0:
                await self.booksTable.update_one(
                    {'ISBN': mIsbn},
                    {'$inc': {'quantity': 1}}
                )
                status = True
                code = 4076
                message = 'Book returned successfully'
            else:
                message = 'Failed to return book'
                code = 4084
                raise Exception
        
        except:
            if not message:
                code = 5000
                message = 'There is some issue or any internal server error'
                raise Exception
        
        response = {
            'code': code,
            'message': message,
            'status': status
        }
        try:
            if len(result):
                response['result'] = result
            print('Return Book', response) 
            self.write(response)
            self.finish()
        except:
            message = 'There is some issue'
            code = 4124
            raise Exception