import motor.motor_asyncio

class Connection:
    try:
        client = motor.motor_asyncio.AsyncIOMotorClient('mongodb://127.0.0.1:27017')
        print('Database Connection OK')
        db = client['lmsBase']
        print(db)
    except Exception as e:
        print('Error:', e)
