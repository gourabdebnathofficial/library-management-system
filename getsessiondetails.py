from datetime import datetime
import tornado.web
from bson.objectid import ObjectId
from database import Connection
import json

from tokenauth import xenProtocol

class SessionHandler(tornado.web.RequestHandler, Connection):
    userTable = Connection.db['users']
    sessionTable = Connection.db['session']

    @xenProtocol
    async def get(self):
        code = 4000
        status = False
        result = []
        message = ''

        try:
            mUserId = self.get_argument('user_id', None)
            if not mUserId:
                code = 4511
                message = "User ID is required"
                raise ValueError("User ID is required")

            query = {'user_id': mUserId}
            print(f"Query: {query}")

            aggregation_pipeline = [
                {'$match': query},
                {'$lookup': {
                    'from': 'users',
                    'localField': 'user_id',
                    'foreignField': '_id',
                    'as': 'user_details'
                }},
                {'$addFields': {
                    'user_details': {'$first': '$user_details'}
                }},
                {'$project': {
                    '_id': {'$toString': '$_id'},
                    'userId': {'$toString': '$user_id'},
                    'name': '$user_details.name',
                    'login_time': 1,
                    'logout_time': 1,
                    'duration': 1,
                }}
            ]

            cursor = self.sessionTable.aggregate(aggregation_pipeline)
            async for session in cursor:
                session['login_time'] = format_timestamp(session.get('login_time'))
                session['logout_time'] = format_timestamp(session.get('logout_time'))
                session['duration'] = format_duration(session.get('duration'))
                result.append(session)
                print(f"Session: {session}")

            if result:
                message = 'Found'
                code = 2000
                status = True
            else:
                message = 'No data found for the given user_id'
                code = 4002

        except ValueError as ve:
            message = str(ve)
        except Exception as e:
            if not message:
                message = 'Internal server error'
                code = 5010

        response = {
            'code': code,
            'message': message,
            'status': status,
        }

        if result:
            response['result'] = result

        self.set_header('Content-Type', 'application/json')
        self.write(response)
        await self.finish()

def format_timestamp(timestamp):
    try:
        if timestamp is None:
            return None
        dt_object = datetime.fromtimestamp(timestamp)
        return dt_object.strftime("%A, %d %B %Y, %H:%M:%S")
    except Exception as e:
        print(f"Error formatting timestamp: {e}")
        return "Invalid Date"

def format_duration(duration):
    try:
        if duration is None:
            return None
        duration_seconds = int(duration)
        hours, remainder = divmod(duration_seconds, 3600)
        minutes, seconds = divmod(remainder, 60)
        return f"{hours}h {minutes}m {seconds}s"
    except Exception as e:
        print(f"Error formatting duration: {e}")
        return "Invalid Duration"
