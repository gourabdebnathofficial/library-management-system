from bson.objectid import ObjectId
import tornado.web
from database import Connection
import time
from tokenauth import xenProtocol

class LogoutHandler(tornado.web.RequestHandler, Connection):
    sessionTable = Connection.db['session']

    @xenProtocol
    async def post(self):
        code = 1000
        status = True
        message = 'User logged out successfully'
        result = []

        try:
            session = await self.sessionTable.find_one({"_id": ObjectId(self.sessionId)})
            if not session:
                code = 4049
                message = 'Session not found'
                status = False
                raise Exception

            if session.get('logout_time'):
                code = 4080
                message = 'User already logged out'
                status = False
                raise Exception

            logout_time = int(time.time())
            duration = logout_time - session['login_time']
            await self.sessionTable.update_one(
                {"_id": ObjectId(self.sessionId)},
                {"$set": {"logout_time": logout_time, "duration": duration, "blacklisted": True}}
            )
            
        except Exception as e:
            if not message:
                message = 'Internal Server Error'
                code = 1005
                status = False

        response = {
            'code': code,
            'message': message,
            'status': status
        }

        self.set_header("Content-Type", "application/json")
        self.write(response)
        self.finish()