import asyncio
import tornado

# API Handler imports
from IssueBookReport.reportGeneration import getIssuebooksExcelHandler
from authentication.forgetpassword.requestResetPass import RequestPasswordResetHandler
from authentication.forgetpassword.reset import ResetPasswordHandler
from authentication.forgetpassword.verifyotp import VerifyOTPHandler
from authentication.signin import SignInHandler
from authentication.signup import SignUpHandler
from book_transaction.issuebook import IssueBookHandler
from book_transaction.returnbook import ReturnBookHandler
from getsessiondetails import SessionHandler
from manage_books.addbook import AddBookHandler
from manage_books.bookPanalty import GetBookPenaltyHandler
from manage_books.booksPieChart import BookCategoryPieChartHandler
from manage_books.deletebook import DeleteBookHandler
from manage_books.getbooks import GetBooksHandler
from manage_books.getissuedbooks import GetIssuedBooksHandler
from manage_books.issuedbooknotificaion import IssuedBookNotificationHandler
from manage_books.updatebook import UpdateBookHandler
from manage_students.addstudent import AddStudentHandler
from manage_students.deletestudent import DeleteStudentHandler
from manage_students.getstudent import GetStudentsHandler
from manage_students.updatestudent import UpdateStudentHandler
from logout import LogoutHandler
from userprofile.checkBookBorrower import CheckAvailabilityHandler
from userprofile.userIssuedBook import GetUserIssuedBooksHandler

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Hello, world")

def make_app():
    return tornado.web.Application([
        (r"/", MainHandler),
        (r'/web/authentication/signin', SignInHandler),
        (r'/web/authentication/signup', SignUpHandler),
        (r'/web/managebooks/addbook', AddBookHandler),
        (r"/web/authentication/request_password_reset", RequestPasswordResetHandler),
        (r"/web/authentication/verify_otp", VerifyOTPHandler),
        (r"/web/authentication/reset_password", ResetPasswordHandler),
        (r'/web/managebooks/updatebook', UpdateBookHandler),
        (r'/web/managebooks/book_type_piechart', BookCategoryPieChartHandler),
        (r'/web/managebooks/deletebook', DeleteBookHandler),
        (r'/web/managebooks/getbooks', GetBooksHandler),
        (r'/web/managebooks/notify_due_books', IssuedBookNotificationHandler),
        (r'/web/transactionbook/getissuebooks', GetIssuedBooksHandler),
        (r'/web/managestudents/addstudent', AddStudentHandler),
        (r'/web/managestudents/updatestudent', UpdateStudentHandler),
        (r'/web/managestudents/deletestudent', DeleteStudentHandler),
        (r'/web/managestudents/getstudent', GetStudentsHandler),
        (r'/web/transactionbook/issuebook', IssueBookHandler),
        (r'/web/transactionbook/returnbook', ReturnBookHandler),
        (r'/web/logout', LogoutHandler),
        (r'/web/sessiondetails', SessionHandler),
        (r'/web/transactionbook/getissuebooksreport', getIssuebooksExcelHandler),
        (r'/web/transactionbook/check_book_borrower', CheckAvailabilityHandler),
        (r'/web/transactionbook/user_issued_book', GetUserIssuedBooksHandler),
        (r'/web/transactionbook/book_fine', GetBookPenaltyHandler),
    ])

async def main():
    app = make_app()
    app.listen(4000)
    await asyncio.Event().wait()

if __name__ == "__main__":
    asyncio.run(main())