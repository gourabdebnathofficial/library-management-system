import json
from bson import ObjectId
import tornado
from database import Connection
from mimetypes import MimeTypes
from uuid import uuid4
import os

from tokenauth import xenProtocol

class AddBookHandler(tornado.web.RequestHandler):
    SUPPORTED_METHODS = ('POST',)
    booksTable = Connection.db['books']
    usersTable = Connection.db['users']
    
    @xenProtocol
    async def post(self):
        code = 4000
        status = False
        result = []
        message = ''
        try:
            user = await self.usersTable.find_one({'_id': ObjectId(self.user_id)})
            if not user:
                message = 'User not found'
                code = 4002
                raise tornado.web.HTTPError(400, reason=message)

            mUserRole = user.get('role')
            if mUserRole != 'admin':
                message = 'Unauthorized access'
                code = 4030
                raise tornado.web.HTTPError(403, reason=message)
            try:
                files = {}
                args = {}
                b = self.request.headers.get('Content-Type')
                tornado.httputil.parse_body_arguments(b, self.request.body, args, files)
                data = json.loads(args['bookdetails'][0].decode('utf-8'))
            except Exception as e:
                code = 1001
                message = f'Expected type in Form_Data or Invalid JSON: {str(e)}'
                raise
            
            mTitle = data.get('title')
            if not mTitle:
                message = 'Title field is required'
                code = 4031
                raise Exception(message)
            
            if type(mTitle) != str:
                message = 'Title must be string'
                code = 4034
                raise Exception(message)

            mAuthor = data.get('author')
            if not mAuthor:
                message = 'Author field is required'
                code = 4031
                raise Exception(message)
            if type(mAuthor) != str:
                message = 'Author must be string'
                code = 4034
                raise Exception(message)

            mIsbn = data.get('ISBN')
            if not mIsbn:
                message = 'ISBN field is required'
                code = 4031
                raise Exception(message)
            
            if type(mIsbn) != str:
                message = 'ISBN must be string'
                code = 4034
                raise Exception(message)

            mPublisher = data.get('publisher')
            if not mPublisher:
                message = 'Publisher field is required'
                code = 4031
                raise Exception(message)
            
            if type(mPublisher) != str:
                message = 'Publisher must be string'
                code = 4034
                raise Exception(message)

            mQuantity = data.get('quantity')
            if not mQuantity:
                message = 'Quantity field is required'
                code = 4031
                raise Exception(message)
            
            if type(mQuantity) != int:
                message = 'Quantity must be integer'
                code = 4034
                raise Exception(message)

            category_name = data.get('category_name')
            if not category_name:
                message = 'Category name is required'
                code = 4031
                raise Exception(message)

            files = self.request.files.get('photo', [])
            images = []
            for index, mphoto in enumerate(files):
                try:
                    if not mphoto:
                        code = 4078
                        message = 'Photo is missing'
                        raise ValueError(f'{index} Photo is missing')
                    mImage = self.save_photo(mphoto, f'photo_{index}')
                    images.append({'filename': mImage})
                except Exception as e:
                    message = str(e)
                    code = 4084
                    raise
            
            if not images:
                message = 'Photos are required'
                code = 4089
                raise ValueError(message)
                

            existing_book = await self.booksTable.find_one({'ISBN': mIsbn})
            if existing_book:
                code = 4050
                message = 'ISBN already exists'
                raise ValueError(message)
            

            new_book = {
                'title': mTitle,
                'author': mAuthor,
                'ISBN': mIsbn,
                'publisher': mPublisher,
                'quantity': mQuantity,
                'images': images,
                'category_name': category_name
            }
            
            book_insert_result = await self.booksTable.insert_one(new_book)
            if book_insert_result.inserted_id:
                status = True
                code = 4072
                message = 'Book added successfully'
            else:
                message = 'Failed to insert book data'
                code = 4081
                raise ValueError(message)
        
        except Exception as e:
            if not message:
                code = 5000
                message = f'There is some issue or an internal server error: {str(e)}'
        
        response = {
            'code': code,
            'message': message,
            'status': status
        }
        try:
            if result:
                response['result'] = result
            print('Add Book', response) 
            self.write(response)
            self.finish()
        except Exception as e:
            message = f'There is some issue: {str(e)}'
            code = 4124
            response = {
                'code': code,
                'message': message,
                'status': False
            }
            self.write(response)
            self.finish()
    
    def save_photo(self, photo, key):
        unique_id = str(uuid4())
        mime_type, _ = MimeTypes().guess_type(photo['filename'])
        extension = MimeTypes().guess_extension(mime_type)
        file_name = f"{unique_id}{extension}"
        
        if not os.path.exists("../uploads/"):
            os.makedirs("../uploads/")
        
        with open("../uploads/" + file_name, 'wb') as output_file:
            output_file.write(photo['body'])
        return file_name
