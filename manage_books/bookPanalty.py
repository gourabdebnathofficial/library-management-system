import datetime
from bson import ObjectId
from database import Connection
import tornado.web
from tokenauth import xenProtocol

class GetBookPenaltyHandler(tornado.web.RequestHandler, Connection):

    SUPPORTED_METHODS = ('GET')
    issuedBooksTable = Connection.db['issued_books']
    userTable = Connection.db['users']  # Replace with actual collection name

    async def get(self):
        code = 4000
        status = False
        message = ''
        result = []
        total = 0

        try:
            current_date = datetime.datetime.now()
            issued_books_cursor = self.issuedBooksTable.find({})

            async for issued_book in issued_books_cursor:
                return_date_timestamp = issued_book.get('return_date')
                if return_date_timestamp is None:
                    continue

                return_date = datetime.datetime.fromtimestamp(return_date_timestamp)
                
                if current_date == return_date:
                    continue

                if current_date > return_date:
                    days_overdue = (current_date - return_date).days
                else:
                    days_overdue = 0  

                if days_overdue > 0:
                    penalty = days_overdue * 20
                    penalty_str = f'Rs {penalty}'

                    user_data = await self.userTable.find_one({'library_id': issued_book.get('library_id')})
                    if user_data:
                        user_name = user_data.get('name')
                    else:
                        user_name = 'Unknown'

                    result.append({
                        'ISBN': issued_book.get('ISBN'),
                        'library_id': issued_book.get('library_id'),
                        'name': user_name,
                        'days_overdue': days_overdue,
                        'penalty': penalty_str
                    })

                    total += 1

            if result:
                result = sorted(result, key=lambda x: x['days_overdue'])

                for item in result:
                    item['days_overdue'] = f"{item['days_overdue']} day"
                
                status = True
                code = 2000
                message = 'Penalties calculated successfully'
            else:
                code = 2001
                message = 'No overdue books found'

        except tornado.web.HTTPError as e:
            code = e.status_code
            message = e.reason
            self.write_error(code, message)
            return
        except Exception as e:
            code = 5000
            message = 'There is some issue or an internal server error'
            self.log_exception(e)
            self.write_error(code, message)
            return

        response = {
            'code': code,
            'message': message,
            'status': status,
            'total_overdue_books': total,
            'result': result
        }

        try:
            print('Get Book Penalty', response)
            self.write(response)
            self.finish()
        except Exception as e:
            code = 4124
            message = 'There is some issue'
            self.log_exception(e)
            self.write_error(code, message)

    def write_error(self, code, message):
        response = {
            'code': code,
            'message': message,
            'status': False
        }
        self.write(response)
        self.finish()
