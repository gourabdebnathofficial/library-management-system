from bson import ObjectId
from database import Connection
import tornado

class BookCategoryPieChartHandler(tornado.web.RequestHandler):
    SUPPORTED_METHODS = ('GET')
    booksTable = Connection.db['books']
    usersTable = Connection.db['users']

    async def get(self):
        code = 4000
        status = False
        message = ''
        result = []

        try:

            pipeline = [
                {"$group": {"_id": "$category_name", "count": {"$sum": 1}}}
            ]
            categories = await self.booksTable.aggregate(pipeline).to_list(length=None)
            
            result = [{"category": cat["_id"], "count": cat["count"]} for cat in categories]

            if result:
                status = True
                code = 2000
                message = 'Category data retrieved successfully'
            else:
                code = 4040
                message = 'No categories found'

        except tornado.web.HTTPError as e:
            code = e.status_code
            message = e.reason
        except Exception as e:
            code = 5000
            message = 'There is some issue or an internal server error'
            self.set_status(500)
            self.write({'code': code, 'message': message, 'status': status})
            self.finish()
            return

        response = {
            'code': code,
            'message': message,
            'status': status,
            'result': result if result else []
        }
        print(response)
        self.set_header("Content-Type", "application/json")
        self.write(response)
        self.finish()

