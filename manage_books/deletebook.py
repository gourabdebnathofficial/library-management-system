from bson import ObjectId
import tornado
from database import Connection
from tokenauth import xenProtocol


class DeleteBookHandler(tornado.web.RequestHandler):
    SUPPORTED_METHODS = ('DELETE')
    booksTable = Connection.db['books']
    usersTable = Connection.db['users']
    
    @xenProtocol
    async def delete(self):
        code = 4000
        status = False
        result = []
        message = ''
        
        try:
            user = await self.usersTable.find_one({'_id': ObjectId(self.user_id)})
            if not user:
                message = 'User not found'
                code = 4002
                raise tornado.web.HTTPError(400, reason=message)

            mUserRole = user.get('role')
            if mUserRole != 'admin':
                message = 'Unauthorized access'
                code = 4030
                raise tornado.web.HTTPError(403, reason=message)
            mIsbn = self.get_argument('ISBN')
            
            
            book = await self.booksTable.find_one({'ISBN': mIsbn})
            if not book:
                code = 4029
                message = 'Wrong ISBN number'
                raise Exception
            
            delete_result = await self.booksTable.delete_one({'ISBN': mIsbn})
            if delete_result.deleted_count > 0:
                status = True
                code = 4074
                message = 'Book deleted successfully'
            else:
                message = 'Failed to delete book'
                code = 4083
                raise Exception
        
        except Exception:
            if not len(message):
                code = 5000
                message = 'There is some issue or any internal server error'
        
        response = {
            'code': code,
            'message': message,
            'status': status
        }
        try:
            if len(result):
                response['result'] = result
            print('Delete Book', response) 
            self.write(response)
            self.finish()
        except:
            message = 'There is some issue'
            code = 4124
            raise Exception
