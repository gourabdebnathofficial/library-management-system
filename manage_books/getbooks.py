from bson import ObjectId
from database import Connection
import tornado
from tokenauth import xenProtocol

class GetBooksHandler(tornado.web.RequestHandler):
    SUPPORTED_METHODS = ('GET')
    booksTable = Connection.db['books']
    usersTable = Connection.db['users']

    @xenProtocol
    async def get(self):
        code = 4000
        status = False
        message = ''
        result = []
        total = 0

        try:
            user = await self.usersTable.find_one({'_id': ObjectId(self.user_id)})
            if not user:
                message = 'User not found'
                code = 4002
                raise tornado.web.HTTPError(400, reason=message)

            mUserRole = user.get('role')
            if mUserRole != 'admin':
                message = 'Unauthorized access'
                code = 4030
                raise tornado.web.HTTPError(403, reason=message)
            
            try:
                skip = int(self.get_argument("skip", 0))
                limit = int(self.get_argument("limit", 10))
            except ValueError:
                skip = 0
                limit = 10

            search = self.get_argument('search', None)
            
            query = {}
            if search:
                query['$or'] = [
                    {'title': {'$regex': search, '$options': 'i'}},
                    {'author': {'$regex': search, '$options': 'i'}},
                    {'ISBN': {'$regex': search, '$options': 'i'}},
                    {'publisher': {'$regex': search, '$options': 'i'}},
                    {'category_name': {'$regex': search, '$options': 'i'}}
                ]

            total = await self.booksTable.count_documents(query)
            pagination = self.booksTable.find(query).skip(skip).limit(limit)

            async for book in pagination:
                for img in book.get('images', []):
                    img['link'] = f'http://10.10.10.100/uploads/{img.get("filename")}'

                result.append({
                    'title': book.get('title', ''),
                    'author': book.get('author', ''),
                    'ISBN': book.get('ISBN', ''),
                    'publisher': book.get('publisher', ''),
                    'category_name': book.get('category_name', ''),
                    'quantity': book.get('quantity', ''),
                    'images': book.get('images', [])
                })

            if result:
                status = True
                code = 2000
                message = 'Books retrieved successfully'
            else:
                code = 4040
                message = 'No books found'

        except tornado.web.HTTPError as e:
            code = e.status_code
            message = e.reason
        except Exception as e:
            code = 5000
            message = 'There is some issue or an internal server error'
            self.set_status(500)
            self.write({'code': code, 'message': message, 'status': status})
            self.finish()
            return

        response = {
            'code': code,
            'message': message,
            'status': status,
            'total': total,
            'result': result if result else []
        }

        self.write(response)
        self.finish()
