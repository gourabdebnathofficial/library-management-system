import datetime

from bson import ObjectId
from database import Connection
import tornado.web

from tokenauth import xenProtocol

class GetIssuedBooksHandler(tornado.web.RequestHandler, Connection):

    SUPPORTED_METHODS = ('GET')
    issuedBooksTable = Connection.db['issued_books']
    usersTable = Connection.db['users']
    bookTable = Connection.db['books']
    
    @xenProtocol
    async def get(self):
        code = 4000
        status = False
        message = ''
        result = []
        total = 0

        try:
            user = await self.usersTable.find_one({'_id': ObjectId(self.user_id)})
            if not user:
                message = 'User not found'
                code = 4002
                raise tornado.web.HTTPError(400, reason=message)

            mUserRole = user.get('role')
            if mUserRole != 'admin':
                message = 'Unauthorized access'
                code = 4030
                raise tornado.web.HTTPError(403, reason=message)
            try:
                skip = int(self.get_argument("skip", 0))
                limit = int(self.get_argument("limit", 10))
            except ValueError:
                skip = 0
                limit = 10

            search = self.get_argument('search', None)

            query = {}
            if search:
                query['$or'] = [
                    {'ISBN': {'$regex': search, '$options': 'i'}},
                    {'library_id': {'$regex': search, '$options': 'i'}}
                ]

            total = await self.issuedBooksTable.count_documents(query)
            pagination = self.issuedBooksTable.find(query).skip(skip).limit(limit)

            async for book in pagination:
                book_details = await self.bookTable.find_one({'ISBN': book.get('ISBN')})
                user_details = await self.usersTable.find_one({'library_id': book.get('library_id')})

                result.append({
                    'ISBN': book.get('ISBN'),
                    'title': book_details.get('title') if book_details else 'Unknown',
                    'library_id': book.get('library_id'),
                    'name': user_details.get('name') if user_details else 'Unknown',
                    'issue_date': datetime.datetime.fromtimestamp(book.get('issue_date')).strftime('%Y-%m-%d'),
                    'return_date': datetime.datetime.fromtimestamp(book.get('return_date')).strftime('%Y-%m-%d')
                })

            if result:
                status = True
                code = 2000
                message = 'Issued Books retrieved successfully'
            else:
                code = 4040
                message = 'No Issued books found'
        
        except Exception as e:
            code = 5000
            message = 'There is some issue or an internal server error'
            self.log_exception(e)
            self.write_error(code, message)
            return

        response = {
            'code': code,
            'message': message,
            'status': status,
            'total': total
        }

        if result:
            response['result'] = result

        try:
            print('Get Issued Book', response)
            self.write(response)
            self.finish()
        except Exception as e:
            code = 4124
            message = 'There is some issue'
            self.log_exception(e)
            self.write_error(code, message)
    
    def write_error(self, code, message):
        response = {
            'code': code,
            'message': message,
            'status': False
        }
        self.write(response)
        self.finish()
    
    def log_exception(self, exception):
        print(f"Exception occurred: {exception}")
