import json
from datetime import datetime, timedelta
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from bson import ObjectId
from database import Connection
import tornado.ioloop
from apscheduler.schedulers.tornado import TornadoScheduler

# Email server configuration
SMTP_SERVER = 'smtp.gmail.com'
SMTP_PORT = 587
SMTP_USERNAME = 'gourabdebnathofficial@gmail.com'
SMTP_PASSWORD = 'pecu sexm maej ldla'
FROM_EMAIL = 'gourabdebnathofficial@gmail.com'
FROM_NAME = 'Library Notification'

class IssuedBookNotificationHandler(tornado.web.RequestHandler):
    SUPPORTED_METHODS = ('GET')
    issuedBooksTable = Connection.db['issued_books']
    usersTable = Connection.db['users']
    booksTable = Connection.db['books']
    
    async def get(self):
        code = 4000
        status = False
        result = []
        message = ''
        try:
            current_date = datetime.now()
            notification_date = current_date + timedelta(days=2)
            notification_timestamp = notification_date.timestamp()

            issued_books = self.issuedBooksTable.find({
                'return_date': {'$lte': notification_timestamp},
                'returned': False
            })

            async for book in issued_books:
                user = await self.usersTable.find_one({'library_id': book['library_id']})
                if user:
                    book_details = await self.booksTable.find_one({'ISBN': book['ISBN']})
                    if book_details:
                        penalty_info = await self.calculate_penalty_info(book)
                        result.append({
                            'ISBN': book['ISBN'],
                            'book_name': book_details.get('title'),
                            'library_id': book['library_id'],
                            'student_name': user['name'],
                            'return_date': datetime.fromtimestamp(book['return_date']).strftime('%Y-%m-%d'),
                            'notification': 'Return date is in 2 days',
                            'penalty_info': penalty_info,
                            'email': user['email']  # Assuming 'email' field in the users collection
                        })

            status = True
            code = 2000
            if not result:
                message = 'No notifications'
            else:
                message = 'Notifications fetched successfully'
                self.send_notification_emails(result)

        except Exception as e:
            if not message:
                code = 5000
                message = str(e)

        response = {
            'code': code,
            'message': message,
            'status': status
        }
        if result:
            response['result'] = result

        self.write(response)
        self.finish()

    async def calculate_penalty_info(self, book):
        current_date = datetime.now()
        return_date = datetime.fromtimestamp(book['return_date'])
        if current_date > return_date:
            days_overdue = (current_date - return_date).days
            penalty = days_overdue * 20
            penalty_str = f'Rs {penalty}'
            return {
                'days_overdue': days_overdue,
                'penalty': penalty_str
            }
        else:
            return None

    def send_notification_emails(self, notifications):
        for notification in notifications:
            to_email = notification.get('email')
            if to_email:
                subject = "Library Book Return Notification"
                body = f"Dear {notification['student_name']},\n\n" \
                       f"You have a book '{notification['book_name']}' (ISBN: {notification['ISBN']}) " \
                       f"due date for return was on {notification['return_date']}.\n\n"

                penalty_info = notification.get('penalty_info')
                if penalty_info:
                    body += f"Days Overdue: {penalty_info['days_overdue']}\n"
                    body += f"Penalty: {penalty_info['penalty']}\n"
                    body += f"For this book '{notification['book_name']}' you need to pay the penalty amount.\n\n"
                else:
                    body += "Please return the book by the due date.\n\n"

                body += "Thank you,\nLibrary\n\n"

                msg = MIMEMultipart()
                msg['From'] = FROM_NAME
                msg['To'] = to_email
                msg['Subject'] = subject
                msg.attach(MIMEText(body, 'plain'))

                try:
                    server = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
                    server.starttls()
                    server.login(SMTP_USERNAME, SMTP_PASSWORD)
                    server.sendmail(FROM_EMAIL, to_email, msg.as_string())
                    server.quit()
                except Exception as e:
                    print(f"Failed to send email to {to_email}: {str(e)}")

async def send_notification_emails():
    issuedBooksTable = Connection.db['issued_books']
    usersTable = Connection.db['users']
    booksTable = Connection.db['books']

    current_date = datetime.now()
    notification_date = current_date + timedelta(days=2)
    notification_timestamp = notification_date.timestamp()

    issued_books = issuedBooksTable.find({
        'return_date': {'$lte': notification_timestamp},
        'returned': False
    })

    notifications = []
    async for book in issued_books:
        user = await usersTable.find_one({'library_id': book['library_id']})
        book_details = await booksTable.find_one({'ISBN': book['ISBN']})
        if user and book_details:
            penalty_info = await calculate_penalty_info(book)
            notifications.append({
                'ISBN': book['ISBN'],
                'book_name': book_details.get('title'),
                'library_id': book['library_id'],
                'student_name': user['name'],
                'return_date': datetime.fromtimestamp(book['return_date']).strftime('%Y-%m-%d'),
                'notification': 'Return date is in 2 days',
                'penalty_info': penalty_info,
                'email': user['email']
            })

    if notifications:
        handler = IssuedBookNotificationHandler()
        await handler.send_notification_emails(notifications)

async def calculate_penalty_info(book):
    current_date = datetime.now()
    return_date = datetime.fromtimestamp(book['return_date'])
    if current_date > return_date:
        days_overdue = (current_date - return_date).days
        penalty = days_overdue * 20
        penalty_str = f'Rs {penalty}'
        return {
            'days_overdue': days_overdue,
            'penalty': penalty_str
        }
    else:
        return None

if __name__ == "__main__":
    scheduler = TornadoScheduler()
    scheduler.add_job(send_notification_emails, 'interval', days=1)
    scheduler.start()

    tornado.ioloop.IOLoop.current().start()
