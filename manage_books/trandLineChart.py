from datetime import datetime, timedelta
from database import Connection
import tornado.web

class BookTrendDataHandler(tornado.web.RequestHandler, Connection):

    SUPPORTED_METHODS = ('GET',)
    issuedBooksTable = Connection.db['issued_books']

    async def get(self):
        code = 4000
        status = False
        message = ''
        result = []

        try:
            end_date = datetime.now()
            start_date = end_date - timedelta(days=30)

            pipeline = [
                {
                    "$match": {
                        "issue_date": {"$gte": start_date.timestamp(), "$lt": end_date.timestamp()}
                    }
                },
                {
                    "$group": {
                        "_id": {
                            "day": {"$dayOfMonth": {"$toDate": {"$toLong": {"$multiply": ["$issue_date", 1000]}}}},
                            "month": {"$month": {"$toDate": {"$toLong": {"$multiply": ["$issue_date", 1000]}}}},
                            "year": {"$year": {"$toDate": {"$toLong": {"$multiply": ["$issue_date", 1000]}}}}
                        },
                        "issued_count": {"$sum": 1},
                        "returned_count": {"$sum": {"$cond": [{"$eq": ["$return_date", None]}, 0, 1]}}
                    }
                },
                {
                    "$sort": {
                        "_id.year": 1,
                        "_id.month": 1,
                        "_id.day": 1
                    }
                }
            ]
            aggregation_result = await self.issuedBooksTable.aggregate(pipeline).to_list(None)

            if not aggregation_result:
                code = 4040
                message = 'No book trends found'
                raise Exception

            result = [{
                'date': datetime(year=res['_id']['year'], month=res['_id']['month'], day=res['_id']['day']).strftime('%Y-%m-%d'),
                'issued_count': res['issued_count'],
                'returned_count': res['returned_count']
            } for res in aggregation_result]

            status = True
            code = 2000
            message = 'Book trend data retrieved successfully'

        except tornado.web.HTTPError as e:
            code = e.status_code
            message = e.reason
        except Exception as e:
            code = 5000
            message = 'There is some issue or an internal server error'
            self.log_exception(e)

        response = {
            'code': code,
            'message': message,
            'status': status,
            'result': result
        }

        try:
            print('Book Trend Data', response)
            self.write(response)
        except Exception as e:
            code = 4124
            message = 'There is some issue'
            self.log_exception(e)
            self.write_error(code, message)
        finally:
            self.finish()

    def write_error(self, code, message, **kwargs):
        response = {
            'code': code,
            'message': message,
            'status': False
        }
        self.write(response)
        self.finish()
