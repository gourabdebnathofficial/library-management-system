import json

from bson import ObjectId
from database import Connection
import tornado

from tokenauth import xenProtocol

class UpdateBookHandler(tornado.web.RequestHandler):
    SUPPORTED_METHODS = ('PUT')
    booksTable = Connection.db['books']
    usersTable = Connection.db['users']
    @xenProtocol
    async def put(self):
        code = 4000
        status = False
        result = []
        message = ''
        
        try:
            user = await self.usersTable.find_one({'_id': ObjectId(self.user_id)})
            if not user:
                message = 'User not found'
                code = 4002
                raise tornado.web.HTTPError(400, reason=message)

            mUserRole = user.get('role')
            if mUserRole != 'admin':
                message = 'Unauthorized access'
                code = 4030
                raise tornado.web.HTTPError(403, reason=message)
            try:
                self.request.arguments = json.loads(self.request.body.decode())
            except json.JSONDecodeError:
                code = 1001
                message = 'Invalid JSON'
                raise Exception
            
            mIsbn = self.request.arguments.get('ISBN')
            if not mIsbn:
                message = 'ISBN field is required'
                code = 4031
                raise Exception
            if type(mIsbn) != str:
                message = 'ISBN must be string'
                code = 4034
                raise Exception
            
            book = await self.booksTable.find_one({'ISBN': mIsbn})
            if not book:
                code = 4029
                message = 'Wrong ISBN number'
                raise Exception
            
            update_fields = {}
            
            mTitle = self.request.arguments.get('title')
            if not mTitle:
                message = 'Title field is required'
                code = 4031
                raise Exception
            if type(mTitle) != str:
                message = 'Title must be string'
                code = 4034
                raise Exception
            update_fields['title'] = mTitle
            
            mAuthor = self.request.arguments.get('author')
            if not mAuthor:
                message = 'Authon field is required'
                code = 4031
                raise Exception
            if type(mAuthor) != str:
                message = 'Author must be string'
                code = 4034
                raise Exception
            update_fields['author'] = mAuthor
            
            mPublisher = self.request.arguments.get('publisher')
            if not mPublisher:
                message = 'Publisher field is required'
                code = 4031
                raise Exception
            if type(mPublisher) != str:
                message = 'Publisher must be string'
                code = 4034
                raise Exception
            update_fields['publisher'] = mPublisher
            
            mQuantity = self.request.arguments.get('quantity')
            if not mQuantity:
                message = 'Quantity field is required'
                code = 4031
                raise Exception
            if type(mQuantity) != int:
                message = 'Quantity must be integer'
                code = 4034
                raise Exception
            update_fields['quantity'] = mQuantity
            
            if not update_fields:
                code = 4030
                message = 'No valid fields to update'
                raise Exception
            

            mCategory = self.request.arguments.get('category_name')
            if not mCategory:
                message = 'Category name field is required'
                code = 4031
                raise Exception
            if type(mCategory) != str:
                message = 'Category name must be integer'
                code = 4034
                raise Exception
            update_fields['category_name'] = mCategory
            
            if not update_fields:
                code = 4030
                message = 'No valid fields to update'
                raise Exception

            update_result = await self.booksTable.update_one(
                {'ISBN': mIsbn},
                {'$set': update_fields}
            )
            if update_result.modified_count > 0:
                status = True
                code = 4073
                message = 'Book updated successfully'
            else:
                message = 'Failed to update book data or no changes made'
                code = 4082
                raise RuntimeError(message)
        
        except Exception:
            if not len(message):
                code = 5000
                message = 'There is some issue or any internal server error'
        
        response = {
            'code': code,
            'message': message,
            'status': status
        }
        try:
            if len(result):
                response['result'] = result
            print('Update Book', response) 
            self.write(response)
            self.finish()
        except:
            message = 'There is some issue'
            code = 4124
            raise Exception
