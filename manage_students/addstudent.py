import json
from bson import ObjectId
import tornado.web
import bcrypt
from database import Connection
from tokenauth import xenProtocol

class AddStudentHandler(tornado.web.RequestHandler):
    SUPPORTED_METHODS = ('POST')
    usersTable = Connection.db['users']
    async def get_user_role(self, user_id):
        user = await self.usersTable.find_one({'_id': ObjectId(user_id)})
        if user:
            return user.get('role')
        return None
    
    @xenProtocol
    async def post(self):
        code = 4000
        status = False
        result = []
        message = ''
        
        try:
            user = await self.usersTable.find_one({'_id': ObjectId(self.user_id)})
            if not user:
                message = 'User not found'
                code = 4002
                raise tornado.web.HTTPError(400, reason=message)

            mUserRole = user.get('role')
            if mUserRole != 'admin':
                message = 'Unauthorized access'
                code = 4030
                raise tornado.web.HTTPError(403, reason=message)
            try:
                self.request.arguments = json.loads(self.request.body.decode())
            except json.JSONDecodeError:
                code = 1001
                message = 'Invalid JSON'
                raise Exception
            
            mLibId = self.request.arguments.get('library_id')
            if not mLibId:
                code = 4024
                message = 'Library ID field is missing'
                raise Exception
            if type(mLibId) != str:
                message = 'Library ID should be string'
                code = 4030
                raise Exception
            
            mStuName = self.request.arguments.get('name')
            if not mStuName:
                code = 4024
                message = 'Name field is missing'
                raise Exception
            if type(mStuName) != str:
                message = 'Name should be string'
                code = 4030
                raise Exception
            
            mStuEmail = self.request.arguments.get('email')
            if not mStuEmail:
                code = 4024
                message = 'Email field is missing'
                raise Exception
            if type(mStuEmail) != str:
                message = 'Email should be string'
                code = 4030
                raise Exception
            
            mStuPass = self.request.arguments.get('password')
            if not mStuPass:
                code = 4024
                message = 'Password field is missing'
                raise Exception
            if type(mStuPass) != str:
                message = 'Password should be string'
                code = 4030
                raise Exception
            
            mStuAddr = self.request.arguments.get('address')
            if not mStuAddr:
                code = 4024
                message = 'Address field is missing'
                raise Exception
            if type(mStuAddr) != str:
                message = 'Address should be string'
                code = 4030
                raise Exception
            
            existing_user_by_id = await self.usersTable.find_one({'library_id': mLibId})
            if existing_user_by_id:
                code = 4050
                message = 'Library ID already exists'
                raise Exception
            
            existing_user_by_email = await self.usersTable.find_one({'email': mStuEmail})
            if existing_user_by_email:
                code = 4058
                message = 'Email already exists'
                raise Exception
            
            hashed_password = bcrypt.hashpw(mStuPass.encode('utf-8'), bcrypt.gensalt()).decode('utf-8')
            
            new_user = {
                'library_id': mLibId,
                'name': mStuName,
                'email': mStuEmail,
                'password': hashed_password,
                'address': mStuAddr
            }
            
            user_insert_result = await self.usersTable.insert_one(new_user)
            if user_insert_result.inserted_id:
                status = True
                code = 4072
                message = 'User add successful'
            else:
                message = 'Failed to insert user data'
                code = 4081
                raise Exception
        except Exception:
            if not message:
                code = 5000
                message = 'There is some issue or any internal error'
        
        response = {
            'code': code,
            'message': message,
            'status': status
        }
        try:
            if len(result):
                response['result'] = result
            print('Add  User', response) 
            self.write(response)
            self.finish()
        except:
            message = 'There is some issue'
            code = 4124
            raise Exception
