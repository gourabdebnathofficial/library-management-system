import json

from bson import ObjectId
from database import Connection
import tornado

from tokenauth import xenProtocol


class DeleteStudentHandler(tornado.web.RequestHandler):
    SUPPORTED_METHODS = ('DELETE')
    studentsTable = Connection.db['students']
    usersTable = Connection.db['users']
    
    
    @xenProtocol
    async def delete(self):
        code = 4000
        status = False
        result = []
        message = ''
        try:
            user = await self.usersTable.find_one({'_id': ObjectId(self.user_id)})
            if not user:
                message = 'User not found'
                code = 4002
                raise tornado.web.HTTPError(400, reason=message)

            mUserRole = user.get('role')
            if mUserRole != 'admin':
                message = 'Unauthorized access'
                code = 4030
                raise tornado.web.HTTPError(403, reason=message)
            mStuLibId = self.get_argument('library_id')
            if not mStuLibId:
                code = 4024
                message = 'Library ID field is missing'
                raise Exception
            if type(mStuLibId) != str:
                message = 'Library ID should be string'
                code = 4030
                raise Exception
            
            book = await self.usersTable.find_one({'library_id': mStuLibId})
            if not book:
                code = 4029
                message = 'Wrong library_id number'
                raise Exception
            
            delete_result = await self.usersTable.delete_one({'library_id': mStuLibId})
            if delete_result.deleted_count > 0:
                status = True
                code = 4074
                message = 'Student account deleted successfully'
            else:
                message = 'Failed to delete'
                code = 4083
                raise Exception
        
        except Exception:
            if not len(message):
                code = 5000
                message = 'There is some issue or any internal server error'
        
        response = {
            'code': code,
            'message': message,
            'status': status
        }
        try:
            if len(result):
                response['result'] = result
            print('Delete Student', response) 
            self.write(response)
            self.finish()
        except:
            message = 'There is some issue'
            code = 4124
            raise Exception
