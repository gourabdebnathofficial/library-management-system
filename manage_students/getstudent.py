import json
from bson import ObjectId
import tornado.web
from tornado.ioloop import IOLoop
from database import Connection
from tokenauth import xenProtocol

class GetStudentsHandler(tornado.web.RequestHandler):
    SUPPORTED_METHODS = ('GET')
    usersTable = Connection.db['users']
    
    
    @xenProtocol
    async def get(self):
        code = 4000
        status = False
        message = ''
        result = []
        total = 0


        try:
            user = await self.usersTable.find_one({'_id': ObjectId(self.user_id)})
            if not user:
                message = 'User not found'
                code = 4002
                raise tornado.web.HTTPError(400, reason=message)

            mUserRole = user.get('role')
            print(mUserRole)
            if mUserRole != 'admin':
                message = 'Unauthorized access'
                code = 4030
                raise tornado.web.HTTPError(403, reason=message)
            skip = int(self.get_argument("skip", 0))
            limit = int(self.get_argument("limit", 10))
            search = self.get_argument('search', None)
        except Exception as e:
            self.set_status(400)
            self.write({
                'code': 4000,
                'message': f'Invalid arguments: {str(e)}',
                'status': False
            })
            return

        try:
            query = {
                '$or': [
                    {'role': {'$ne': 'admin'}}
                ]
            }
            if search:
                query['$or'].extend([
                    {'library_id': {'$regex': search, '$options': 'i'}},
                    {'name': {'$regex': search, '$options': 'i'}},
                    {'email': {'$regex': search, '$options': 'i'}}
                ])

            total = await self.usersTable.count_documents(query)
            pagination = self.usersTable.find(query).skip(skip).limit(limit)

            async for student in pagination:
                result.append({
                    'library_id': student.get('library_id', ''),
                    'name': student.get('name', ''),
                    'email': student.get('email', ''),
                    'address': student.get('address', '')
                })

            if result:
                status = True
                code = 2000
                message = 'Students retrieved successfully'
            else:
                code = 4040
                message = 'No students found'

        except Exception as e:
            self.set_status(500)
            self.write({
                'code': 5000,
                'message': f'There is some issue or an internal server error: {str(e)}',
                'status': False
            })
            return

        response = {
            'code': code,
            'message': message,
            'status': status,
            'result': result if result else [],
            'total': total
        }

        self.write(response)
        self.finish()
