import json

from bson import ObjectId
from database import Connection
import tornado

from tokenauth import xenProtocol

class UpdateStudentHandler(tornado.web.RequestHandler):
    SUPPORTED_METHODS = ('POST')
    usersTable = Connection.db['users']
    
    
    @xenProtocol   
    async def post(self):
        code = 4000
        status = False
        result = []
        message = ''
        try:
            user = await self.usersTable.find_one({'_id': ObjectId(self.user_id)})
            if not user:
                message = 'User not found'
                code = 4002
                raise tornado.web.HTTPError(400, reason=message)

            mUserRole = user.get('role')
            if mUserRole != 'admin':
                message = 'Unauthorized access'
                code = 4030
                raise tornado.web.HTTPError(403, reason=message)
            try:
                self.request.arguments = json.loads(self.request.body.decode())
            except json.JSONDecodeError:
                code = 4000
                message = 'Invalid JSON'
                raise Exception(message)
            
            mStuLibId = self.request.arguments.get('library_id')
            if not mStuLibId:
                code = 4024
                message = 'Library ID field is missing'
                raise Exception(message)
            if type(mStuLibId) != str:
                message = 'Library ID should be string'
                code = 4030
                raise Exception(message)
            
            mStuAddr = self.request.arguments.get('address')
            if not mStuAddr:
                code = 4024
                message = 'Address field is missing'
                raise Exception
            
            if type(mStuAddr) != str:
                message = 'Address should be string'
                code = 4030
                raise Exception

            existing_student = await self.usersTable.find_one({'library_id': mStuLibId})
            if not existing_student:
                code = 4040
                message = 'Student not found'
                raise Exception(message)

            update_fields = {}

            mStuName = self.request.arguments.get('name')
            if mStuName:
                if type(mStuName) != str:
                    message = 'Name should be string'
                    code = 4030
                    raise Exception(message)
                update_fields['name'] = mStuName

            mStuEmail = self.request.arguments.get('email')
            if mStuEmail:
                if type(mStuEmail) != str:
                    message = 'Email should be string'
                    code = 4030
                    raise Exception(message)
                update_fields['email'] = mStuEmail
            
            mStuAddr = self.request.arguments.get('address')
            if mStuAddr:
                if type(mStuAddr) != str:
                    message = 'Address should be string'
                    code = 4030
                    raise Exception(message)
                update_fields['address'] = mStuAddr

            if not update_fields:
                code = 4001
                message = 'No fields to update'
                raise Exception(message)

            update_result = await self.usersTable.update_one(
                {'library_id': mStuLibId},
                {'$set': update_fields}
            )

            if update_result.modified_count > 0:
                status = True
                code = 2001
                message = 'Student updated successfully'
            else:
                message = 'No changes applied'
                code = 2002

        except:
            if not message:
                code = 5000
                message = 'Internal server error'
                raise Exception


        response = {
            'code': code,
            'message': message,
            'status': status
        }
        try:
            if len(result):
                response['result'] = result
            print('Update Student', response) 
            self.write(response)
            self.finish()
        except:
            message = 'There is some issue'
            code = 4124
            raise Exception
