from bson import ObjectId
import tornado.web
import jwt
from database import Connection
from datetime import datetime
import json

SECRET_KEY = "Xlayer.in"

sessionTable = Connection.db['session']
tokenUsageLogsTable = Connection.db['token_usage_logs']

def xenProtocol(method):
    async def wrapper(self, *args, **kwargs):
        auth_header = self.request.headers.get("Authorization")
        if not auth_header:
            self.set_status(401)
            self.write({
                'code': 4026,
                'message': "Authorization header missing",
                'status': False
            })
            self.finish()
            return
        
        try:
            token = auth_header.split()[1]
            decoded = jwt.decode(token, SECRET_KEY, algorithms=["HS256"])
            self.sessionId = decoded.get("_id")
            self.user_id = decoded.get("user_id")
            
            session = await sessionTable.find_one({"_id": ObjectId(self.sessionId)})
            if not session or session.get('blacklisted'):
                self.set_status(401)
                self.write({
                    'code': 4077,
                    'message': 'Invalid or blacklisted token',
                    'status': False
                })
                self.finish()
                return
        except jwt.ExpiredSignatureError:
            self.set_status(401)
            self.write({
                'code': 4012,
                'message': 'Token has expired, Login again!',
                'status': False
            })
            self.finish()
            return
        except jwt.InvalidTokenError:
            self.set_status(401)
            self.write({
                'code': 4020,
                'message': 'Invalid token',
                'status': False
            })
            self.finish()
            return
        except Exception as e:
            self.set_status(500)
            self.write({
                'code': 5000,
                'message': str(e),
                'status': False
            })
            self.finish()
            return

        try:
            body = self.request.body.decode('utf-8')
        except UnicodeDecodeError:
            body = self.request.body.hex()

        try:
            json_body = json.loads(body)
        except json.JSONDecodeError:
            json_body = {}

        all_arguments = {k: self.get_argument(k) for k in self.request.arguments}
        if self.request.method in ['POST', 'PUT', 'DELETE'] and json_body:
            all_arguments.update(json_body)

        log_entry = {
            "user_id": self.user_id,
            "session_id": self.sessionId,
            "operation": method.__name__,
            "timestamp": datetime.utcnow(),
            "request_data": {
                "path": self.request.path,
                "headers": dict(self.request.headers),
                "arguments": all_arguments
            }
        }
        await tokenUsageLogsTable.insert_one(log_entry)

        await method(self, *args, **kwargs)
    return wrapper
