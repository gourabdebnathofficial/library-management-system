import datetime

from bson import ObjectId
from database import Connection
import tornado.web

from tokenauth import xenProtocol

class CheckAvailabilityHandler(tornado.web.RequestHandler, Connection):

    SUPPORTED_METHODS = ('GET')
    issuedBooksTable = Connection.db['issued_books']
    usersTable = Connection.db['users']
    bookTable = Connection.db['books']
    
    # @xenProtocol
    async def get(self):
        code = 4000
        status = False
        message = ''
        result = []
        total_students = 0

        try:
            search = self.get_argument('search', None)
            if not search or len(search) < 3:
                message = 'Search query is required and must be at least 3 characters long'
                code = 4001
                raise tornado.web.HTTPError(400, reason=message)

            book_query = {'$or': [
                {'ISBN': {'$regex': search, '$options': 'i'}},
                {'title': {'$regex': search, '$options': 'i'}}
            ]}
            book_details = await self.bookTable.find_one(book_query)

            if not book_details:
                message = 'Book not found'
                code = 4040
                raise tornado.web.HTTPError(404, reason=message)

            ISBN = book_details.get('ISBN')

            issued_books = self.issuedBooksTable.find({'ISBN': ISBN})

            students = set()
            async for book in issued_books:
                students.add(book.get('library_id'))

            total_students = len(students)

            if total_students > 0:
                status = True
                code = 2000
                message = 'Students retrieved successfully'
                
                for library_id in students:
                    student = await self.usersTable.find_one({'library_id': library_id})
                    if student:
                        result.append({
                            'library_id': library_id,
                            'name': student.get('name', 'Unknown')
                        })
            else:
                code = 4041
                message = 'No students found with the specified book'

        except tornado.web.HTTPError as e:
            self.set_status(e.status_code)
            code = e.status_code
            message = e.reason
            self.log_exception(e)
            self.write_error(code, message)
            return
        except Exception as e:
            code = 5000
            message = 'There is some issue or an internal server error'
            self.log_exception(e)
            self.write_error(code, message)
            return

        response = {
            'code': code,
            'message': message,
            'status': status,
            'total_students': total_students,
            'students': result
        }

        try:
            print('Get Students With Book', response)
            self.write(response)
            self.finish()
        except Exception as e:
            code = 4124
            message = 'There is some issue'
            self.log_exception(e)
            self.write_error(code, message)
    
    def write_error(self, code, message):
        response = {
            'code': code,
            'message': message,
            'status': False
        }
        self.write(response)
        self.finish()
    
    def log_exception(self, exception):
        print(f"Exception occurred: {exception}")
