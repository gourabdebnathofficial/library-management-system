import datetime
from bson import ObjectId
from database import Connection
import tornado.web
from tokenauth import xenProtocol

class GetUserIssuedBooksHandler(tornado.web.RequestHandler):
    SUPPORTED_METHODS = ('GET')
    issuedBooksTable = Connection.db['issued_books']
    usersTable = Connection.db['users']
    booksTable = Connection.db['books']
    
    @xenProtocol
    async def get(self):
        code = 4000
        status = False
        result = []
        message = ''
        
        try:
            if not self.user_id:
                code = 401
                message = 'Unauthorized access'
                raise Exception
            
            user = await self.usersTable.find_one({'_id': ObjectId(self.user_id)})
            if not user:
                code = 404
                message = 'User not found'
                raise Exception
            
            mLibId = user.get('library_id')
            if not mLibId:
                code = 404
                message = 'Library ID not found for user'
                raise Exception
            
            issued_books = await self.issuedBooksTable.find({'library_id': mLibId}).to_list(length=None)
            
            if issued_books:
                status = True
                code = 200
                message = 'Issued books retrieved successfully'
                for book in issued_books:
                    book.pop('library_id', None)
                    book.pop('name', None)
                    
                    if '_id' in book:
                        book['_id'] = str(book['_id'])
                    if 'issue_date' in book:
                        book['issue_date'] = datetime.datetime.fromtimestamp(book['issue_date']).strftime('%Y-%m-%d')
                    if 'return_date' in book:
                        book['return_date'] = datetime.datetime.fromtimestamp(book['return_date']).strftime('%Y-%m-%d')
                    if 'ISBN' in book:
                        book_details = await self.booksTable.find_one({'ISBN': book['ISBN']})
                        if book_details and 'title' in book_details:
                            book['title'] = book_details['title']
                    else:
                        book['title'] = 'Title not found'
                result = issued_books
            else:
                code = 404
                message = 'No issued books found for the given library ID'
        
        except Exception as e:
            if not message:
                code = 500
                message = f'Internal server error: {str(e)}'
        
        response = {
            'code': code,
            'message': message,
            'status': status,
            'result': result
        }
        
        self.write(response)
        self.finish()
